local crys_etherium = terumet.register_crystal{
    suffix='eth',
    color='#7b96e8',
    name='Crystallized Etherium',
    cooking_result='ethereal:etherium_dust'
}
terumet.register_vulcan_result('ethereal:etherium_ore', crys_etherium)
terumet.register_vulcan_result('ethereal:stone_with_etherium_ore', crys_etherium)
terumet.register_vulcan_result("ethereal:etherium_dust", "terumet:item_cryst_eth", -1, false, true)

local crystal_mrv = 190

terumet.register_repair_material("ethereal:crystal_ingot", crystal_mrv)

terumet.register_repairable_item("ethereal:pick_crystal", crystal_mrv*3+20)
terumet.register_repairable_item("ethereal:axe_crystal", crystal_mrv*3+20)
terumet.register_repairable_item("ethereal:shovel_crystal", crystal_mrv+20)
terumet.register_repairable_item("ethereal:sword_crystal", crystal_mrv*2+10)
