if minetest.get_modpath("df_farming") then
    terumet.register_entropic_node({
        node = "df_farming:cavern_fungi",
        hu_per_s = 1500,
        extract_time = 6.0,
        change_to = "df_farming:dead_fungus",
    })

    terumet.register_entropic_node({
        node = "df_farming:dead_fungus",
        hu_per_s = 1500,
        extract_time = 3.0,
        change_to = "air",
    })
end

if minetest.get_modpath("df_mapitems") then
    terumet.register_entropic_node({
        node = "df_mapitems:dry_flowstone",
        hu_per_s = 1000,
        extract_time = 3.0,
        change_to = "default:cobble",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:wet_flowstone",
        hu_per_s = 1000,
        extract_time = 3.0,
        change_to = "default:cobble",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:glow_worm",
        hu_per_s = 1500,
        extract_time = 3.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:dirt_with_cave_moss",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:sand_scum",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:sand",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:dirt_with_pebble_fungus",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:dirt_with_stillworm",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:spongestone",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:rock_rot",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:stone",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:cobble_with_floor_fungus",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:cobble",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:cobble_with_floor_fungus_fine",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:cobble",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:ice_with_hoar_moss",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:ice",
    })

    terumet.register_entropic_node({
        node = "df_mapitems:snareweed",
        hu_per_s = 1500,
        extract_time = 6.0,
        change_to = "default:dirt",
    })
end

if minetest.get_modpath("df_primordial_items") then
    terumet.register_entropic_node({
        node = "df_primordial_items:glow_orb_hanging",
        hu_per_s = 1500,
        extract_time = 6.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "df_primordial_items:dirt_with_mycelium",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "df_primordial_items:dirt_with_jungle_grass",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "df_primordial_items:plant_matter",
        hu_per_s = 2000,
        extract_time = 3.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "df_primordial_items:packed_roots",
        hu_per_s = 2000,
        extract_time = 3.0,
        change_to = "air",
    })
end

if minetest.get_modpath("df_trees") then
    terumet.register_entropic_node({
        node = "df_trees:blood_thorn_spike",
        hu_per_s = 2000,
        extract_time = 4.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "df_trees:blood_thorn_spike_dead",
        hu_per_s = 2000,
        extract_time = 2.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "df_trees:tunnel_tube_fruiting_body",
        hu_per_s = 1500,
        extract_time = 20.0,
        change_to = "air",
    })
end

if minetest.get_modpath("df_underworld_items") then
    terumet.register_entropic_node({
        node = "df_underworld_items:glowstone",
        hu_per_s = 5000,
        extract_time = 8.0,
        change_to = "air",
    })

end

if minetest.get_modpath("ethereal") then
    terumet.register_entropic_node({
        node = "ethereal:bamboo_dirt",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "ethereal:jungle_dirt",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "ethereal:grove_dirt",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "ethereal:prairie_dirt",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "ethereal:crystal_dirt",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "ethereal:mushroom_dirt",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "ethereal:fiery_dirt",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "ethereal:gray_dirt",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:dirt",
    })

    terumet.register_entropic_node({
        node = "ethereal:quicksand",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:sand",
    })

    terumet.register_entropic_node({
        node = "ethereal:quicksand2",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "default:sand",
    })

    terumet.register_entropic_node({
        node = "ethereal:mushroom",
        hu_per_s = 1500,
        extract_time = 5.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:mushroom_pore",
        hu_per_s = 1500,
        extract_time = 3.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:bush1",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:bush2",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:bush3",
        hu_per_s = 1000,
        extract_time = 1.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:seaweed",
        hu_per_s = 1000,
        extract_time = 3.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:coral2",
        hu_per_s = 1000,
        extract_time = 3.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:coral3",
        hu_per_s = 1000,
        extract_time = 3.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:coral4",
        hu_per_s = 1000,
        extract_time = 3.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:coral5",
        hu_per_s = 1000,
        extract_time = 3.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:dry_shrub",
        hu_per_s = 1500,
        extract_time = 3.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:snowygrass",
        hu_per_s = 1500,
        extract_time = 3.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "ethereal:crystalgrass",
        hu_per_s = 1500,
        extract_time = 3.0,
        change_to = "air",
    })
end

if minetest.get_modpath("mine_gas") then
    terumet.register_entropic_node({
        node = "mine_gas:gas",
        hu_per_s = 2000,
        extract_time = 1.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "mine_gas:gas_seep",
        hu_per_s = 1500,
        extract_time = 10.0,
        change_to = "default:stone",
    })

    terumet.register_entropic_node({
        node = "mine_gas:gas_wisp",
        hu_per_s = 10000,
        extract_time = 1.0,
        change_to = "air",
    })
end

if minetest.get_modpath("oil") then
    terumet.register_entropic_node({
        node = "oil:oil_source",
        hu_per_s = 1500,
        extract_time = 60.0,
        change_to = "air",
    })

    terumet.register_entropic_node({
        node = "oil:oil_flowing",
        hu_per_s = 500,
        extract_time = 1.0,
        change_to = "air",
    })
end
