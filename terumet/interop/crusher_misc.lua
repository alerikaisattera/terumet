-- add crushing recipes for various mods if they are active

local function add_crush(item, result)
    terumet.options.crusher.recipes[item] = result
end


if minetest.get_modpath('bushes') then
    add_crush('bushes:BushLeaves1', 'terumet:item_dust_bio')
    add_crush('bushes:BushLeaves2', 'terumet:item_dust_bio')
end

if minetest.get_modpath('dryplants') then
    add_crush('dryplants:grass', 'terumet:item_dust_bio')
end

if minetest.get_modpath('vines') then
    add_crush('vines:vines', 'terumet:item_dust_bio')
end

if minetest.get_modpath('farming') then
    -- small crops turn into 2 biomatter
    local small_crops = {
        'wheat', 'peas', 'barley', 'beans', 'pepper', 'beetroot', 'chili_pepper', 'blueberries',
        'cucumber', 'grapes', 'garlic', 'onion', 'pea_pod', 'pineapple_ring', 'pineapple_top', 'potato',
        'raspberries', 'tomato', 'corn', 'rhubarb', 'artichoke', 'asparagus', 'blackberry', 'cabbage', 'carrot',
        'coffee_beans', 'eggplant', 'lettuce', 'parsley', 'pepper', 'pepper_yellow', 'pepper_red', 'soy_pod',
        'spinach', 'strawberry', 'vanilla'
    }

    for _,crop in ipairs(small_crops) do
        local crop_id = 'farming:'..crop
        if minetest.registered_items[crop_id] then
            add_crush(crop_id, 'terumet:item_dust_bio 2')
        end
    end

    -- big crops turn into 5 biomatter
    local big_crops = {
        'pumpkin_8', 'pineapple', 'melon_8'
    }

    for _,crop in ipairs(big_crops) do
        local crop_id = 'farming'..crop
        if minetest.registered_items[crop_id] then
            add_crush(crop_id, 'terumet:item_dust_bio 5')
        end
    end
end

if minetest.get_modpath('df_farming') then
    add_crush('df_farming:cavern_fungi', 'terumet:item_dust_bio')
    
    local crops = {
        'df_farming:cave_wheat', 'df_farming:quarry_bush_leaves', 'df_farming:sweet_pods'
    }
    
    for _,crop in ipairs(crops) do
        add_crush(crop, 'terumet:item_dust_bio 2')
    end
end

if minetest.get_modpath('df_primordial_items') then
    local grasses = {
        'df_primordial_items:fern_1', 'df_primordial_items:fern_2', 'df_primordial_items:jungle_grass_1', 'df_primordial_items:jungle_grass_2', 'df_primordial_items:jungle_grass_3',
        'df_primordial_items:jungle_mushroom_1', 'df_primordial_items:fungal_grass_1', 'df_primordial_items:fungal_grass_2', 'df_primordial_items:glow_orb', 'df_primordial_items:glow_orb_stalks', 
    }
    
    for _,grass in ipairs(grasses) do
        add_crush(grass, 'terumet:item_dust_bio')
    end
    
    add_crush('df_primordial_items:jungle_mushroom_2', 'terumet:item_dust_bio 2')
    
    add_crush('df_primordial_items:primordial_fruit', 'terumet:item_dust_bio 3')
    add_crush('df_primordial_items:glowtato', 'terumet:item_dust_bio 3')
    
    add_crush('df_primordial_items:plant_matter', 'terumet:item_dust_bio 5')
    add_crush('df_primordial_items:packed_roots', 'terumet:item_dust_bio 5')
end

if minetest.get_modpath('ethereal') then
    local fruits = {
        'ethereal:banana', 'ethereal:orange', 'ethereal:lemon', 'ethereal:olive'
    }
    
    for _,fruit in ipairs(fruits) do
        add_crush(fruit, 'terumet:item_dust_bio')
    end
    
    add_crush('ethereal:wild_onion_plant', 'terumet:item_dust_bio')
    add_crush('ethereal:strawberry', 'terumet:item_dust_bio')
    
    add_crush('ethereal:fire_flower', 'ethereal:fire_dust 4')
    
    add_crush('ethereal:seaweed', 'terumet:item_dust_bio')
end

if minetest.get_modpath('bonemeal') then
    add_crush('bonemeal:bone', 'bonemeal:bonemeal 4')
    add_crush('bones:bones', 'bonemeal:bonemeal 8')
end
