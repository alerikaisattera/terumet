
local crys_mithril = terumet.register_crystal{
    suffix='mith',
    color='#6161d5',
    name='Crystallized Mithril',
    cooking_result='moreores:mithril_ingot'
}
terumet.register_vulcan_result('moreores:mithril_lump', crys_mithril)
terumet.register_vulcan_result('moreores:mineral_mithril', crys_mithril, 1)
terumet.register_vulcan_result("moreores:mithril_ingot", crys_mithril, -1, false, true)

local crys_silver = terumet.register_crystal{
    suffix='silv',
    color='#d3fffb',
    name='Crystallized Silver',
    cooking_result='moreores:silver_ingot'
}
terumet.register_vulcan_result('moreores:silver_lump', crys_silver)
terumet.register_vulcan_result('moreores:mineral_silver', crys_silver, 1)
terumet.register_vulcan_result("moreores:silver_ingot", crys_silver, -1, false, true)


terumet.crystal_ids.mitril = crys_mithril
terumet.crystal_ids.silver = crys_silver

local silver_mrv = 45
local mithril_mrv = 110

terumet.register_repair_material("moreores:silver_ingot", silver_mrv)
terumet.register_repair_material("moreores:mithril_ingot", mithril_mrv)

terumet.register_repairable_item("moreores:pick_silver", silver_mrv*3)
terumet.register_repairable_item("moreores:axe_silver", silver_mrv*3)
terumet.register_repairable_item("moreores:sword_silver", silver_mrv*2)
terumet.register_repairable_item("moreores:shovel_silver", silver_mrv)
terumet.register_repairable_item("moreores:pick_mithril", mithril_mrv*3)
terumet.register_repairable_item("moreores:axe_mithril", mithril_mrv*3)
terumet.register_repairable_item("moreores:sword_mithril", mithril_mrv*2)
terumet.register_repairable_item("moreores:shovel_mithril", mithril_mrv)

if minetest.registered_items["farming:scythe_mithril"] ~= nil then
    terumet.register_repairable_item("farming:scythe_mithril", mithril_mrv*3)
end

terumet.register_alloy_recipe{result='basic_materials:brass_ingot 3', flux=0, time=4.0, input={'default:copper_lump 2', 'moreores:silver_lump'}}
terumet.register_alloy_recipe{result='basic_materials:brass_ingot 3', flux=0, time=8.0, input={'default:copper_ingot 2', 'moreores:silver_ingot'}}
terumet.register_alloy_recipe{result='basic_materials:brass_block 3', flux=0, time=40.0, input={'default:copperblock 2', 'moreores:silver_block'}}
terumet.register_alloy_recipe{result='basic_materials:brass_ingot 3', flux=0, time=2.0, input={'terumet:item_cryst_copper 2', crys_silver}}
