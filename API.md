# Terumet mod API

## Heatline/Reinforced Blocks
Registering a block so that it may be used to create reinforced or heatline versions:

`terumet.register_convertible_block(id, unique_code, exclude)`

**id**: The string node id of your block that you want to support. (i.e. 'default:stone')

**unique_code**: A short string to identify the block type. Anything is valid but duplicates will overwrite previously registered blocks. (ex: a unique_code of 'myblk' will create 'terumet:reinf_block_myblk1' for Reinforced, 'terumet:reinf_block_myblk2' for Double-reinforced, etc.)

**exclude**: An optional table of which conversions to exclude your block from. {heatline=true} means ONLY register reinforced versions, while {reinforced=true} means ONLY register heatline versions.

## Repair Materials
Registering an item so it may be placed in an Equipment Reformer to provide repair material value:

`terumet.register_repair_material(id, value)`

**id**: The string item id of your item that you want to allow as repair material (i.e. 'default:copper_ingot')

**value**: The value of 1 item in repair material value. For reference, here are the default values for 1 ingot of each Minetest and Terumetal material:
* Raw Terumetal = 10
* Minetest Steel = 10
* Terucopper = 20
* Minetest Bronze = 30
* Terusteel = 40
* Teruchalcum = 60
* Terugold = 80
* Minetest Mese = 90
* Minetest Diamond = 100
* Coreglass = 120

Therefore to repair as much as 1 Minetest diamond, you require 10 ingots of raw terumetal, and so forth.

## Repairable Tools
Registering a tool so it may be placed in an Equipment Reformer and repaired:

`terumet.register_repairable_item(id, full_rm)`

**id**: The string item id of your tool that you want to make repairable (i.e. 'default:pick_steel'). *IMPORTANT*: The item in question SHOULD be a tool--created by minetest.register_tool()--and NOT anything else.

**full_rm**: The value in repair material to fully repair a tool from maximum wear. This is a ratio, so to repair a tool at 50% wear, it will cost 50% of this value in repair material to fully repair. The default tool values are calculated by the material value above times the number of of those items required to craft the item (i.e. Pickaxes are x3, Axes are x3, Swords are x2, and Shovels are x1 -- A Minetest Mese Pickaxe's full_rm is 90 x 3 = 270 repair material for full repair)

## Alloy Smelter Recipe
Registering a new Terumetal Alloy Smelter recipe in a mod without modifying options.lua:

`terumet.register_alloy_recipe({result, inputs, time, flux})` - argument is table with below keys

**result**: Itemstring of the resulting item of this recipe.

**inputs**: Table of up to 4 itemstrings describing stacks that must be consumed.

**time**: Float value in seconds the process takes at standard speed

**flux**: Integer value of Terumetal Flux units required to be consumed from the tank to process

## Vacuum Oven Recipe

`terumet.register_vacoven_recipe({input, time, results})` - argument is table with below keys

**input**: Itemstring of the source item

**time**: Float value in seconds the process takes at standard speed

**results**: Table of up to 4 itemstrings describing stacks that are produced.

## Crystallizer Recipes and Crystallized Items

Registering a crystal item:

`terumet.register_crystal({suffix, color, name, cooking_result})` - argument is table with below keys

**suffix**: String suffix to use for crystallized item - 'XYZ' will result in 'terumet:item_cryst_XYZ' being registered

**color**: Colorspec (see minetest lua_api.txt for description) to apply to colorize crystallized item

**name**: Full name of crystallized item

**cooking_result**: Itemstack string of what is created by cooking the crystallized item. *NOTE*: the cooking_result and source should NOT be the same unless you wish to allow infinite creation of this item via the Vulcanizer!

`terumet.register_vulcan_result(source, result, count_modifier, specialized, limited)` - add a Crystal Vulcanizer recipe, outputs 2 of resulting item by default

**source**: Itemstring of source item consumed by Crystal Vulcanizer to create crystallized items

**result**: Itemsring of the resulting item

**count_modifier**: Added to amount of resulting items

**specialized**: If true, this item can be processed by Crystal Vulcanizer that has Terumetal Specialization upgrade

**limited**: If true, this recipe is not affected by Crystallization upgrade

## Expansion Crusher recipes

`terumet.register_crusher_recipe(source, result)`

**source**: Itemstring of source item

**result**: Itemsring of the resulting item

## Lava Melter recipes

`terumet.register_lava_melting(source, hups)`

**source**: Itemstring of source item

**hups**: Heat units per second used by Lava Melter when melting this item

## Node Affected by EEE Heater
Registering the effects of a node being drained by the EEE Heater in a mod without modifying options.lua:

`terumet.register_entropic_node({node, hu_per_s, extract_time, [change_to]})` - argument is table with below keys

**node**: String id of node

**hu_per_s**: Integer amount of Heat Units extracted per second

**extract_time**: Float time in seconds to fully extract node

**[change_to]** (optional): String id of what node to change the node into after extraction (ex: change_to='air' to destroy node after extraction)

## Node that can be excavated by an Ore-cutting saw

`function terumet.register_ore_cutting(ore)`

**ore**: Itemstring of node

## Custom Heat-based Machine

`terumet.register_heat_machine( id, {data...} )`

(lots to document, coming soon... see [tmapisample mod](https://gitlab.com/alerikaisattera/terumet/-/tree/main/tmapisample) subfolder for example mod implementing this with copious comments)


Taken from https://github.com/Terumoc/terumet/wiki/Modding-API with modifications
